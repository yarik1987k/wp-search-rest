<?php 

// yeroslav: Search queries tasks
// Start Task
function search_results_posts_callback( $query ) {
	$search_query = get_search_query();
	$query->set('s', $search_query); 
	$query->set('post_type', 'post');
}
add_action( 'elementor/query/search_results_posts', 'search_results_posts_callback' );

function search_results_recipe_callback( $query ) {
	$search_query = get_search_query();
	$query->set('s', $search_query); 
	$query->set('post_type', 'recipe');
}
add_action( 'elementor/query/search_results_recipe', 'search_results_recipe_callback' );

 add_filter( 'relevanssi_hits_filter', 'search_result_types' );
function search_result_types( $hits ) {
    global $hns_search_result_type_counts, $wp_query, $rlv_doing_this_already;
    
    if ( ! $rlv_doing_this_already && 'any' !== $wp_query->query_vars['post_type'] ) {
    	$copy_query                          = $wp_query;
    	$copy_query->query_vars['post_type'] = 'any';
    	$rlv_doing_this_already              = true;
    	relevanssi_do_query( $copy_query );
    	return $hits;
    }
    
    $types = array();
    if ( ! empty( $hits ) ) {
        foreach ( $hits[0] as $hit ) {
            $types[ $hit->post_type ]++;
        }
    }
 
    $hns_search_result_type_counts = $types;
    return $hits;
}

// End Task

// Custom search input
 
function enqueue_scripts() {
    if (!wp_script_is('ajax-search-custom', 'enqueued')) {
    // -> Here we need to point to the file of search.js
        wp_enqueue_script('ajax-search-custom', get_stylesheet_directory_uri() . '/ajax-search.js', array('jquery'), '6.092323230920d03d1', true);
    }
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

function localize_ajax_url() {
    wp_localize_script('ajax-search-custom', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'localize_ajax_url');




// Search html -> shorcode
function ajax_search_shortcode() {
    // Generate a unique identifier using a random number
    $unique_id = rand(1000, 9999);

    ob_start();
    ?>
    <div class="ajax-search-container" id="ajax-search-container-<?php echo $unique_id; ?>">
		<div class="mobile-search-icon">
			<button>
				<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="100" height="100" viewBox="0 0 50 50">
<path d="M 21 3 C 11.621094 3 4 10.621094 4 20 C 4 29.378906 11.621094 37 21 37 C 24.710938 37 28.140625 35.804688 30.9375 33.78125 L 44.09375 46.90625 L 46.90625 44.09375 L 33.90625 31.0625 C 36.460938 28.085938 38 24.222656 38 20 C 38 10.621094 30.378906 3 21 3 Z M 21 5 C 29.296875 5 36 11.703125 36 20 C 36 28.296875 29.296875 35 21 35 C 12.703125 35 6 28.296875 6 20 C 6 11.703125 12.703125 5 21 5 Z"></path>
</svg>			
			</button>

		</div>
        <div class="input-search-holder">
						 <div class="close-results-mobile">
				 <button>
					 <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="100" height="100" viewBox="0 0 30 30">
    <path d="M 7 4 C 6.744125 4 6.4879687 4.0974687 6.2929688 4.2929688 L 4.2929688 6.2929688 C 3.9019687 6.6839688 3.9019687 7.3170313 4.2929688 7.7070312 L 11.585938 15 L 4.2929688 22.292969 C 3.9019687 22.683969 3.9019687 23.317031 4.2929688 23.707031 L 6.2929688 25.707031 C 6.6839688 26.098031 7.3170313 26.098031 7.7070312 25.707031 L 15 18.414062 L 22.292969 25.707031 C 22.682969 26.098031 23.317031 26.098031 23.707031 25.707031 L 25.707031 23.707031 C 26.098031 23.316031 26.098031 22.682969 25.707031 22.292969 L 18.414062 15 L 25.707031 7.7070312 C 26.098031 7.3170312 26.098031 6.6829688 25.707031 6.2929688 L 23.707031 4.2929688 C 23.316031 3.9019687 22.682969 3.9019687 22.292969 4.2929688 L 15 11.585938 L 7.7070312 4.2929688 C 7.5115312 4.0974687 7.255875 4 7 4 z"></path>
</svg>
				 </button>
			</div>
            <div class="loader-container">
                <div class="loader"></div>
            </div> 
            <input type="text" class="search-input" id="searchInput" placeholder="חיפוש מוצר בחנות">
			        <div class="search-results-container">

			<div class="search-results-container--inner">
				
			</div>
		</div>
        </div>

    </div>
 
    <?php

    return ob_get_clean();
}
add_shortcode('ajax_search', 'ajax_search_shortcode');


function handle_ajax_response() {
    if (isset($_POST['api_data'])) {
        $api_data = $_POST['api_data'];
  
        $grouped_posts = array();
        foreach ($api_data as $post) {
            $post_type = $post['subtype'];  
            if (!isset($grouped_posts[$post_type])) {
                $grouped_posts[$post_type] = array();
            }
            $grouped_posts[$post_type][] = $post;
        }  
         ob_start();
        	include get_stylesheet_directory() . '/templates/search-result-template.php';
 		$html_content = ob_get_clean(); 
        echo json_encode(array('html_content' => $html_content));
    }

    wp_die();  
}
add_action('wp_ajax_handle_ajax_response', 'handle_ajax_response');
add_action('wp_ajax_nopriv_handle_ajax_response', 'handle_ajax_response');


?>