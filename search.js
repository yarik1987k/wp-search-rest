jQuery(document).ready(function($) {
	 
        
    var mobileSearchButton = $('.mobile-search-icon button');
    var closeResultsMobile = $('.close-results-mobile');
    var searchResultsElement = $('.ajax-search-container');
    var searchInput = $('.input-search-holder').find('.search-input');
    var searchTimeout;
    var searchXHR;
    
    
    function updateQuantity(productID, quantity) {
        $('[data-product_id="' + productID + '"]').attr('data-quantity', quantity);

        var href = '?add-to-cart=' + productID + '&quantity=' + quantity;
        $('[data-product_id="' + productID + '"]').attr('href', href);
    }
        $(document).on('click', '.custom_minus', function() {
        var currentQuantity = parseInt($(this).siblings('.qnt-input').val());

        if (currentQuantity > 1) {
            var newQuantity = currentQuantity - 1;
            $(this).siblings('.qnt-input').val(newQuantity);
            updateQuantity($(this).data('productid'), newQuantity);
        }
    });
 
    $(document).on('click', '.custom_plus', function() {
        var currentQuantity = parseInt($(this).siblings('.qnt-input').val());
        var newQuantity = currentQuantity + 1;
        $(this).siblings('.qnt-input').val(newQuantity);
        updateQuantity($(this).data('productid'), newQuantity);
    });
    
    function showLoader() {
        $('.loader-container').addClass('active');
         
    } 
    function hideLoader() {
        $('.loader-container').removeClass('active');
    }
    
    function makeAjaxCall(results, data, term){
        showLoader()
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
             data: {
                action: 'handle_ajax_response',
                api_data: data,
                term: term
            },
            success: function(response) {
    try {
        // Check if the response is not empty
        if (response.trim() !== '') {
            response = JSON.parse(response);
            results.html(response.html_content);
        } else {
            // If the response is empty, handle it accordingly
            console.log('Empty response');
            results.html('יש לך טעם מאוד מיוחד ☺</br>            לא מצאנו מוצרים שמתאימים לחיפוש שלך, כדאי לשנות קצת את הבחירה ולנסות שוב');
        }
    } catch (error) {
        console.error('Error parsing JSON:', error);
        results.html('Error in parsing JSON');
    }

    hideLoader();
}

        });
    }
    
function waitForClass(element, targetClass) {
    return new Promise((resolve, reject) => {
        const observer = new MutationObserver((mutationsList) => {
            for (const mutation of mutationsList) {
                if (mutation.attributeName === 'class') {
                    const currentClasses = mutation.target.classList;
                    if (currentClasses.contains(targetClass)) {
                        resolve();
                        observer.disconnect();
                    }
                }
            }
        });

        const observerConfig = { attributes: true };
        observer.observe(element[0], observerConfig);
    });
}

$(document).on('click', '.results-block .add-to-cart-loop', function() {
    const loaderBtnContainer = $(this).find('.loader-btn-container');
    const cartWidgetSide = $('.cart-widget-side');

    loaderBtnContainer.addClass('active');

    waitForClass(cartWidgetSide, 'wd-opened').then(() => {
        loaderBtnContainer.removeClass('active'); 
    });

    
});

    mobileSearchButton.each(function(){
        $(this).on('click', function(){
            $('.input-search-holder').toggleClass('active-mobile');
            
            $('html').toggleClass('active-search')
            $('.search-input').val('');
        });
    });

    closeResultsMobile.each(function(){
        $(this).on('click', function(){
            $('.input-search-holder').removeClass('active-mobile');
            $('.search-results-container').removeClass('active');
            $('html').removeClass('active-search')
        });
    });

searchInput.each(function () {
$(this).on('input change', function () {
    showLoader();
    var searchTerm = $(this).val();

    clearTimeout(searchTimeout);
    if (searchXHR) {
        searchXHR.abort();
    }

    if (searchTerm.length <= 2) {
        hideLoader();
        var searchResultsContainer = $(this).closest('.input-search-holder').find('.search-results-container');
        searchResultsContainer.removeClass('active'); // Hide results container
        searchResultsContainer.find('.search-results-container--inner').html(''); // Clear search results
        return;
    }

    var searchResultsContainer = $(this).closest('.input-search-holder').find('.search-results-container');

    // Set a new timeout to trigger the AJAX call after a delay (e.g., 500 milliseconds)
    searchTimeout = setTimeout(function () {
        searchXHR = $.ajax({
            url: '/wp-json/wp/v2/search/',
            type: 'GET',
            data: { 
                search: searchTerm
            },
            success: function (data) {
                var resultsElement = searchResultsContainer.find('.search-results-container--inner');

                searchResultsContainer.addClass('active');

                if (searchTerm.length <= 2) {
                    data = [];
                }

                makeAjaxCall(resultsElement, data, searchTerm);
            },
            error: function (xhr, status, error) { 
                console.error('Error in AJAX call:', error);
            }
        });
    }, 500);
});

$(this).on('keydown', function (event) {
console.log('Keydown event triggered');
console.log('Event target:', $(this));		
    if (event.keyCode === 13) {  
        event.preventDefault(); 
        
        // Get the search term
        var searchTerm = $(this).val().trim();
        
        if (searchTerm.length > 0) {
            // Construct the search URL
            var searchUrl = '/?s=' + encodeURIComponent(searchTerm);
            
            // Redirect the user to the search URL
            window.location.href = searchUrl;
        }
    }
    if (event.keyCode === 27) { 
        var searchResultsContainer = $(this).closest('.input-search-holder').find('.search-results-container');
        searchResultsContainer.removeClass('active');
    }
});
});


});
 
document.addEventListener('click', function(event) {
var searchContainer = document.querySelector('.search-results-container');
var searchInput = document.querySelector('.search-input');
var inputHolder = document.querySelector('.input-search-holder');
if(inputHolder){
 if (!inputHolder.contains(event.target)) {
searchContainer.classList.remove('active');
}
}

});

window.addEventListener('scroll', function() {
var searchContainer = document.querySelector('.search-results-container');
if(searchContainer){
if (window.pageYOffset > 700) {
searchContainer.classList.remove('active');
}	   
   }
});
