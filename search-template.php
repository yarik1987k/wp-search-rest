<div class="results-block">
	<?php 
	$search_term = isset($_POST['term']) ? sanitize_text_field($_POST['term']) : '';

	$search_results_url = home_url('/') . '?s=' . $search_term;

	?>
    <?php
    // First, display the "product" post type
    if (isset($grouped_posts['product'])) {
        $title = "מוצרים קשורים";
        ?>
        <div class="column-results">
            <h2><?php echo esc_html($title); ?></h2>
            <ul>
                <?php foreach ($grouped_posts['product'] as $post) {
                    $product = wc_get_product($post['id']);
                    if ($product) { 
				    $is_bundle = get_post_meta($product->get_id(), '_bundle_type', true); 
					$product_type = $product->get_type();
					$product_link = get_permalink($product->get_id());
					$is_in_stock = $product->is_in_stock(); 
					?>
                        <li class="product-grid-item product wd-hover-standard wd-quantity color-scheme-dark type-product post-<?php echo $product->get_id(); ?> status-publish has-post-thumbnail taxable shipping-taxable purchasable product-type-simple" data-id="<?php echo $product->get_id(); ?>">
                            <div class="product-wrapper">
                                <div class="top-section">
                                    <div class="product-element-top wd-quick-shop">
                                        <a href="<?php echo get_permalink($product->get_id()); ?>"><?php echo get_the_post_thumbnail($product->get_id(), 'thumbnail'); ?></a>
                                    </div>
                                    <div class="content-holder">
										<?php if(!$is_in_stock){ echo '<span class="not-in-stock">אזל</span>';}?>
                                        <h3 class="wd-entities-title"> <a href="<?php echo get_permalink($product->get_id()); ?>"><?php echo esc_html($product->get_name()); ?></a></h3>
                                        <div class="holder-price">
											<div class="holder-price__inner">
												<span class="price"><?php echo wc_price($product->get_price()); ?></span>
												                                        <div class="quantity">
                                            <input type="button" value="-" class="custom_minus" data-productid="<?php echo $product->get_id(); ?>">
                                            <input type="number" id="quantity_<?php echo $product->get_id(); ?>" class="input-text qty text qnt-input" value="1" aria-label="כמות המוצר" min="1" max="" name="quantity" step="1" placeholder="" inputmode="numeric" autocomplete="off">
                                            <input type="button" value="+" class="custom_plus" data-productid="<?php echo $product->get_id(); ?>">
                                        </div>
											</div>
                                        <div class="bottom_section">
										<?php if('simple' === $product_type && $is_bundle === ''){?>
                                        <a href="?add-to-cart=<?php echo $product->get_id(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop" data-product_id="<?php echo $product->get_id(); ?>" data-product_sku="<?php echo $product->get_sku(); ?>" aria-label="" aria-describedby="" rel="nofollow"><span>הוספה לסל</span>
									<span class="loader-btn-container">
										<span class="loader-btn">
										</span>
									</span>
									</a>
											<?php } ?>
											<?php if('variable' === $product_type || $is_bundle === 'yes'){?>
											<a href="<?php echo $product_link;?>" data-quantity="1" class="button add_to_cart_button" data-product_id="<?php echo $product->get_id(); ?>" data-product_sku="<?php echo $product->get_sku(); ?>" aria-label="" aria-describedby="" rel="nofollow">
												<span>בחר אפשרויות</span>
								 
											</a>												
											<?php } ?>
                                    </div>

                    </div>
                                    </div>
                                </div>
                                
                            </div>
                        </li>
                    <?php }
                } ?>
            </ul>
        </div>
    <?php }

    // Next, display the other post types
    foreach ($grouped_posts as $post_type => $posts) {
        if ($post_type !== 'product') {
            $title = '';
            switch ($post_type) {
                case "post":
                    $title = "פוסטים קשורים";
                    break;
                case "recipe":
                    $title = "מתכונים קשורים";
                    break;
                default:
                    echo "";
                    break;
            }
            ?>
            <div class="column-results">
                <h2><?php echo esc_html($title); ?></h2>
                <div class="post-results">
                <ul>
                    <?php foreach ($posts as $post) {
                        $post_data = get_post($post['id']);
                        if ($post_data) { ?>
                            <li>
                                <div class="post-result-inner">
                                     
                                    <div class="content">
                                    <h3><?php echo esc_html($post_data->post_title); ?></h3>
                                    <a href="<?php echo get_permalink($post_data->ID); ?>">להמשך קריאה</a>
                                    </div>
                                    
                                </div>
                            </li>
                        <?php }
                    } ?>
                </ul>
                </div>
            </div>
        <?php }
    } ?>
	<div class="all-results">
		<a href="<?php echo $search_results_url;?>">לכל התוצאות</a>
	</div>
</div>
